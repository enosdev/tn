<?php

namespace Source\App\Admin;

use Source\Models\Auth;
use Source\Models\CafeApp\AppPlan;
use Source\Models\CafeApp\AppSubscription;
use Source\Models\Category;
use Source\Models\Post;
use Source\Models\Report\Online;
use Source\Models\User;
use Source\Models\Statistic;

/**
 * Class Dash
 * @package Source\App\Admin
 */
class Dash extends Admin
{
    /**
     * Dash constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     *
     */
    public function dash(): void
    {
        redirect("/".PATH_ADMIN."/dash/home");
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function home(?array $data): void
    {
        //real time access
        if (!empty($data["refresh"])) {
            $list = null;
            $items = (new Online())->findByActive();
            if ($items) {
                foreach ($items as $item) {
                    $list[] = [
                        "dates" => date_fmt($item->created_at, "H\hi") . " - " . date_fmt($item->updated_at, "H\hi"),
                        "user" => ($item->user ? $item->user()->fullName() : "Usuário Comum"),
                        "pages" => $item->pages,
                        "url" => $item->url
                    ];
                }
            }

            echo json_encode([
                "count" => (new Online())->findByActive(true),
                "list" => $list
            ]);
            return;
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Dashboard",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            theme("/assets/images/image.jpg", CONF_VIEW_ADMIN),
            false
        );

        echo $this->view->render("widgets/dash/home", [
            "app" => "dash",
            "head" => $head,
//            "control" => (object)[
//                "subscribers" => (new AppSubscription())->find("pay_status = :s", "s=active")->count(),
//                "plans" => (new AppPlan())->find("status = :s", "s=active")->count(),
//                "recurrence" => (new AppSubscription())->recurrence()
//            ],
            "statistic" =>(object)[
                "user" => (new Statistic())->find("MONTH(created_at) = MONTH(Now())","","SUM(users) as users")->fetch()->users,
                "views" => (new Statistic())->find("MONTH(created_at) = MONTH(Now())","","SUM(views) as views")->fetch()->views,
                "pages" => (new Statistic())->find("MONTH(created_at) = MONTH(Now())","","SUM(pages) as pages")->fetch()->pages
            ],
            "now" => (new Statistic())->find("MONTH(created_at) = MONTH(Now()) && DAY(created_at) = DAY(NOW())","","users,views,pages")->fetch(),
            "blog" => (object)[
                "posts" => (new Post())->find("status = 'post'")->count(),
                "drafts" => (new Post())->find("status = 'draft'")->count(),
                "categories" => (new Category())->find("type = 'post'")->count()
            ],
            "users" => (object)[
                // "users" => (new User())->find("level = 1")->count(),
                "admins" => (new User())->find("level = 6")->count(),
                "colunista" => (new User())->find("level = 7")->count(),
                "editores" => (new User())->find("level = 8")->count()
            ],
            "online" => (new Online())->findByActive(),
            "onlineCount" => (new Online())->findByActive(true)
        ]);
    }

    /**
     *
     */
    public function logoff(): void
    {
        $this->message->success("Você saiu com sucesso {$this->user->first_name}.")->flash();

        Auth::logout();
        redirect("/".PATH_ADMIN."/login");
    }
}