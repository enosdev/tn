<?php

namespace Source\App\Admin;

use Source\Models\Gallery;
use Source\Support\Pager;
use Source\Support\Thumb;
use Source\Support\Upload;
use Source\Models\User;

/**
 * Class Users
 * @package Source\App\Admin
 */
class Gal extends Admin
{
    /**
     * Users constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param array|null $data
     */
    public function home(?array $data): void
    {
        //search redirect
        if (!empty($data["s"])) {
            $s = str_search($data["s"]);
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/gal/home/{$s}/1")]);
            return;
        }

        $search = null;
        $gal = (new Gallery())->find("status != :status","status=trash");

        if (!empty($data["search"]) && str_search($data["search"]) != "all") {
            $search = str_search($data["search"]);
            $gal = (new Gallery())->find("title Like '%{$search}%'");
            if (!$gal->count()) {
                $this->message->info("Sua pesquisa não retornou resultados")->flash();
                redirect("/".PATH_ADMIN."/gal/home");
            }
        }

        $all = ($search ?? "all");
        $pager = new Pager(url("/".PATH_ADMIN."/gal/home/{$all}/"));
        $pager->pager($gal->count(), 32, (!empty($data["page"]) ? $data["page"] : 1));

        $head = $this->seo->render(
            CONF_SITE_NAME . " | Galeria de Fotos",
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/gal/home", [
            "app" => "gal/home",
            "head" => $head,
            "search" => $search,
            "gallery" => $gal->order("date_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function gallery(?array $data): void
    {
        //create
        if (!empty($data["action"]) && $data["action"] == "create") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $galleryCreate = new Gallery();
            $galleryCreate->author = $data["author"];
            $galleryCreate->title = $data["title"];
            $galleryCreate->uri = str_slug($galleryCreate->title);
            $galleryCreate->local = $data["local"];
            $galleryCreate->tag = $data["tag"];
            $galleryCreate->views = 0;
            $galleryCreate->event_date = date_fmt_back($data["event_date"]);
            $galleryCreate->date_at = date_fmt_back($data["date_at"]);
            $galleryCreate->entertainment = $data["entertainment"];
            $galleryCreate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->gallery($files, $galleryCreate->title, 1024);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $galleryCreate->cover = $image;

                $img = explode('.',$image);
                mkdir(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$img[0]}", 0777);
            }

            if (!$galleryCreate->save()) {
                $json["message"] = $galleryCreate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Galeria cadastrada com sucesso...")->flash();
            $json["redirect"] = url("/".PATH_ADMIN."/gal/gallery/{$galleryCreate->id}");

            echo json_encode($json);
            return;
        }

        //update
        if (!empty($data["action"]) && $data["action"] == "update") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $galleryUpdate = (new Gallery())->findById($data["gallery_id"]);

            if (!$galleryUpdate) {
                $this->message->error("Você tentou gerenciar uma galeria que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/gal/home")]);
                return;
            }

            $galleryUpdate->author = $data["author"];
            $galleryUpdate->title = $data["title"];
            $galleryUpdate->uri = str_slug($galleryUpdate->title);
            $galleryUpdate->local = $data["local"];
            $galleryUpdate->tag = $data["tag"];
            $galleryUpdate->event_date = date_fmt_back($data["event_date"]);
            $galleryUpdate->date_at = date_fmt_back($data["date_at"]);
            $galleryUpdate->entertainment = $data["entertainment"];
            $galleryUpdate->status = $data["status"];

            //upload photo
            if (!empty($_FILES["photo"])) {
                if ($galleryUpdate->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryUpdate->cover}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryUpdate->cover}");
                    (new Thumb())->flush($galleryUpdate->cover);
                }

                $files = $_FILES["photo"];
                $upload = new Upload();
                $image = $upload->gallery($files, $galleryUpdate->title, 1024);

                if (!$image) {
                    $json["message"] = $upload->message()->render();
                    echo json_encode($json);
                    return;
                }

                $galleryUpdate->cover = $image;
            }

            if (!$galleryUpdate->save()) {
                $json["message"] = $galleryUpdate->message()->render();
                echo json_encode($json);
                return;
            }

            $this->message->success("Galeria atualizada com sucesso...")->flash();
            echo json_encode(["reload" => true]);
            return;
        }

        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);
            $galleryDelete = (new Gallery())->findById($data["gallery_id"]);

            if (!$galleryDelete) {
                $this->message->error("Você tentou deletar uma galeria que não existe")->flash();
                echo json_encode(["redirect" => url("/".PATH_ADMIN."/gal/home")]);
                return;
            }

            if ($galleryDelete->cover && file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryDelete->cover}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$galleryDelete->cover}");
                (new Thumb())->flush($galleryDelete->cover);
            }

            $img = explode('.',$galleryDelete->cover);
            $pasta = __DIR__ . "/../../../" . CONF_UPLOAD_DIR . "/{$img[0]}/";

            if(is_dir($pasta))
            {
                $diretorio = dir($pasta);
                while($arquivo = $diretorio->read())
                {
                    if(($arquivo != '.') && ($arquivo != '..'))
                    {
                        unlink($pasta.$arquivo);
                    }
                }
                $diretorio->close();
                rmdir($pasta);
            }

            $galleryDelete->destroy();

            $this->message->success("A galeria foi excluída com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/gal/home")]);

            return;
        }

        $galleryEdit = null;
        if (!empty($data["gallery_id"])) {
            $galleryId = filter_var($data["gallery_id"], FILTER_VALIDATE_INT);
            $galleryEdit = (new Gallery())->findById($galleryId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($galleryEdit ? "Editando {$galleryEdit->title}" : "Nova Galeria"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        echo $this->view->render("widgets/gal/gallery", [
            "app" => "gal/gallery",
            "head" => $head,
            "gall" => $galleryEdit,
            "authors" => (new User())->find("level >= :level", "level=5")->fetch(true)
        ]);
    }

    /**
     * @param array|null $data
     * @throws \Exception
     */
    public function photos(?array $data): void
    {
        //deleteAll
        if (!empty($data["action"]) && $data["action"] == "deleteAll"){
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            $check_input = explode(',',$data['check']);

            foreach($check_input as $check){
                if (file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "{$check}")) {
                    unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "{$check}");
                }
            }

            $this->message->success("As images foram excluidas com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/gal/photos/{$data["back"]}")]);
            
            return;
        }
        //delete
        if (!empty($data["action"]) && $data["action"] == "delete") {
            $data = filter_var_array($data, FILTER_SANITIZE_STRIPPED);

            if (file_exists(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "{$data["gallery_id"]}")) {
                unlink(__DIR__ . "/../../../" . CONF_UPLOAD_DIR . "{$data["gallery_id"]}");
            }

            $this->message->success("A image foi excluida com sucesso...")->flash();
            echo json_encode(["redirect" => url("/".PATH_ADMIN."/gal/photos/{$data["back"]}")]);

            return;
        }
        
        $galleryEdit = null;
        if (!empty($data["gallery_id"])) {
            $galleryId = filter_var($data["gallery_id"], FILTER_VALIDATE_INT);
            $galleryEdit = (new Gallery())->findById($galleryId);
        }

        $head = $this->seo->render(
            CONF_SITE_NAME . " | " . ($galleryEdit ? "Editando {$galleryEdit->title}" : "Nova Galeria"),
            CONF_SITE_DESC,
            url("/".PATH_ADMIN),
            url("/".PATH_ADMIN."/assets/images/image.jpg"),
            false
        );

        $pastinha = explode('.',$galleryEdit->cover);
        $past = explode('/',$pastinha[0]);
        $past = array_pop($past);
        $folder = str_replace($past,'',$pastinha[0]);

        $arch = glob(getcwd().'/'.CONF_UPLOAD_DIR.'/'.$folder.$galleryEdit->uri.'/*.*');
        //ordena por ordem decrescente
        // ksort($arch, SORT_STRING);
        krsort($arch, SORT_STRING);

        echo $this->view->render("widgets/gal/photos", [
            "app" => "gal/photos",
            "head" => $head,
            "gall" => $galleryEdit,
            "count" => count($arch),
            "image" => $arch
        ]);
    }

}