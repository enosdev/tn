<?php

namespace Source\App;

use Source\Core\Controller;
use Source\Models\Auth;
use Source\Models\Category;
use Source\Models\Faq\Question;
use Source\Models\Gallery;
use Source\Models\Stopwatch;
use Source\Models\Post;
use Source\Models\Agenda;
use Source\Models\Report\Access;
use Source\Models\Report\Online;
use Source\Models\Top10;
use Source\Models\User;
use Source\Support\Pager;

/**
 * Ett Controller
 * @package Source\App
 */
class Ett extends Controller
{
    /**
     * Ett constructor.
     */
    public function __construct()
    {
        parent::__construct(__DIR__ . "/../../themes/" . CONF_VIEW_THEME . "/");

        (new Access())->report();
        (new Online())->report();
    }

    private function cor()
    {
        return (new Category())->findById(10,'color')->color;
    }
    private function videoModal()
    {
        return (new Post())->findPost("type = :t && category = :c", "t=post&c=36")->order("post_at DESC")->fetch();
    }
    
    /**
     * SITE HOME
     */
    public function home(): void
    {
        $head = $this->seo->render(
             "Entretenimento - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url(),
            theme("/assets/images/share.jpg")
        );

        //seleciona o parent da categoria
        $p_arr = (new Category())->find("id = :p || parent = :p", "p=10")->fetch(true);
        $ids = [];
        foreach($p_arr as $ca){
            $ids[] = $ca->id;
        }
        $i_x = implode(',',$ids);

        //seleciona somernte os vídeos
        $c_arr = (new Category())->find("id = :p || parent = :p", "p=36")->fetch(true);
        $idsc = [];
        foreach($c_arr as $cac){
            $idsc[] = $cac->id;
        }
        $i_c = implode(',',$idsc);
        

        $_7dias =  date('Y-m-d H:i:s', strtotime('-7days'));
        $hoje = date('Y-m-d H:i:s');

        echo $this->view->render("ent/entretenimento", [
            "head" => $head,
            "cor" => (new Category())->findById(10,'color')->color,
            "slideNoticias" => (new Post())
                ->findPost("type = :t && category IN({$i_x})", "t=post")
                ->order("post_at DESC")
                ->limit(5)
                ->fetch(true),
            "galleryDestaque" => (new Gallery())
                ->findGallery("entertainment = :e", "e=yes")
                ->order("date_at DESC")
                ->limit(4)
                ->fetch(true),
            "outrasNoticias" => (new Post())
                ->findPost("type = :t && category IN({$i_x})", "t=post")
                ->order("post_at DESC")
                ->limit(3)
                ->offset(5)
                ->fetch(true),
            "videosEntr" => (new Post())
                ->findPost("type = :t && category IN({$i_c})", "t=post")
                ->order("post_at DESC")
                ->limit(3)
                ->fetch(true),
            //asside begin
            "videosEntrLink" => self::videoModal(),
            "postsRecentes" => (new Post())
                ->findPost("type = :t && category NOT IN(10,35,36)", "t=post")
                ->order("post_at DESC")
                ->limit(5)
                ->fetch(true),
            "maisVisitados" => (new Post())
                ->findPost("type = :t && category IN({$i_x}) && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
                ->order("views DESC")
                ->limit(5)
                ->fetch(true),
            "maisGallery" => (new Gallery())
                ->findGallery("entertainment = :e", "e=yes")
                ->order("date_at DESC")
                ->limit(6)
                ->offset(4)
                ->fetch(true),
            "maisCoberturas" => (new Gallery())
                ->findGallery("entertainment = :e", "e=yes")
                ->order("date_at DESC")
                ->limit(8)
                ->offset(10)
                ->fetch(true),
            "colunista" => (new Post())
                ->findPost('type = :t && author = 27', 't=column')
                ->order("post_at DESC")
                ->fetch(),
            "colunista2" => (new Post())
                ->findPost('type = :t && author = 26', 't=column')
                ->order("post_at DESC")
                ->fetch(),
            "list_agenda" => (new Agenda())
                ->findAgenda('event_date >= NOW()')
                ->order("event_date ASC")
                ->limit(5)
                ->fetch(true),
            "timer" => (new Stopwatch())
                ->findStopwatch("event_at >= NOW()")
                ->fetch(),
            "ultimasEstado" => (new Post())
                ->findPost("type = :t", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            // "gallery" => (new Gallery())
            //     ->findGallery()
            //     ->order('date_at DESC')
            //     ->limit(4)
            //     ->fetch(true),
            "promocao" => (new Post())
                ->findPost('type = :t', 't=promotion')
                ->fetch()
        ]);
    }

    /**
     * SITE ABOUT
     */
    public function about(): void
    {
        $head = $this->seo->render(
            "Descubra o " . CONF_SITE_NAME . " - " . CONF_SITE_DESC,
            CONF_SITE_DESC,
            url("/sobre"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("about", [
            "head" => $head,
            "about" => (new Post())
                ->findPost("type = :t", "t=radio")
                ->fetch()
        ]);
    }

    /**
     * SITE CONTACT
     */
    public function contact(): void
    {
        $head = $this->seo->render(
            "Descubra o " . CONF_SITE_NAME . " - " . CONF_SITE_DESC,
            CONF_SITE_DESC,
            url("/contato"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("contact", [
            "head" => $head,
            "video" => "lDZGl9Wdc7Y",
            "faq" => (new Question())
                ->find("channel_id = :id", "id=1", "question, response")
                ->order("order_by")
                ->fetch(true)
        ]);
    }

    /**
     * SOLITICAR COBERTURA
     */
    public function requestEvent(): void
    {
        $head = $this->seo->render(
            "Solicite uma cobertura no seu evento " . CONF_SITE_NAME . " - " . CONF_SITE_DESC,
            CONF_SITE_DESC,
            url("/solicitar-cobertura"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("solicitar-cobertura", [
            "head" => $head,
            "cor" => self::cor()
        ]);
    }
    

    /**
     * DIVULGUE SEU EVENTO
     */
    public function divulge(): void
    {
        $head = $this->seo->render(
            "Divulgue seu evento " . CONF_SITE_NAME . " - " . CONF_SITE_DESC,
            CONF_SITE_DESC,
            url("/divulgue-seu-evento"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("divulgue-seu-evento", [
            "head" => $head,
            "cor" => self::cor()
        ]);
    }

    /**
     * SITE GALLERY
     * @param array|null $data
     */
    public function galeria(?array $data): void
    {
        $head = $this->seo->render(
            "Coberturas - " . CONF_SITE_NAME,
            "Confira as imagens",
            url("/coberturas"),
            theme("/assets/images/share.jpg")
        );

        $gallery = (new Gallery())->findGallery("entertainment = :e", "e=yes");
        $pager = new Pager(url("/coberturas/p/"));
        $pager->pager($gallery->count(), 16, ($data['page'] ?? 1));

        echo $this->view->render("ent/galeria", [
            "head" => $head,
            "cor" => (new Category())->findByUri("entretenimento")->color,
            "article" => $gallery->order("date_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE GALLERY SEARCH
     * @param array $data
     */
    public function galeriaSearch(array $data): void
    {
        if (!empty($data['s'])) {
            $search = str_search($data['s']);
            // echo json_encode(["redirect" => url("/artigo/buscar/{$search}/1")]);
            redirect("/galeria/buscar/{$search}/1");
            return;
        }

        $search = str_search($data['search']);
        $page = (filter_var($data['page'], FILTER_VALIDATE_INT) >= 1 ? $data['page'] : 1);

        if ($search == "all") {
            redirect("/galeria");
        }

        $head = $this->seo->render(
            "Pesquisa por {$search} - " . CONF_SITE_NAME,
            "Confira os resultados de sua pesquisa para {$search}",
            url("/galeria/buscar/{$search}/{$page}"),
            theme("/assets/images/share.jpg")
        );

        $galleySearch = (new Gallery())->findGallery("title LIKE '%{$search}%'");

        if (!$galleySearch->count()) {
            echo $this->view->render("galeria", [
                "head" => $head,
                "title" => "PESQUISA POR:",
                "search" => $search
            ]);
            return;
        }

        $pager = new Pager(url("/galeria/buscar/{$search}/"));
        $pager->pager($galleySearch->count(), 9, $page);

        echo $this->view->render("galeria", [
            "head" => $head,
            "title" => "PESQUISA POR:",
            "search" => $search,
            "article" => $galleySearch->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE GALLERY POST
     * @param array $data
     */
    public function galeriaPost(array $data): void
    {
        $post = (new Gallery())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            "Confira as imagens",
            url("/fotos/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        $pastinha = explode('.',$post->cover);
        $past = explode('/',$pastinha[0]);
        $past = array_pop($past);
        $folder = str_replace($past,'',$pastinha[0]);

        $arch = glob(getcwd().'/'.CONF_UPLOAD_DIR.'/'.$folder.$post->uri.'/*.*');
        // $pager = new Pager(url("/fotos/{$post->uri}/p/"));
        // $pager->pager(count($arch), 5, ($data['page'] ?? 1));

        // $cont_arch = ($arch == null)? ['no_image'] : $arch ;

        // $atual = ($data['page'] ?? 1);
        // $pagArch = array_chunk($cont_arch, $pager->limit());
        // $resultado  = $pagArch[$atual-1];

        echo $this->view->render("ent/galeria-post", [
            "head" => $head,
            "post" => $post,
            "cor" => (new Category())->findByUri("entretenimento")->color,
            "count" => count($arch),
            "image" => $arch
            // "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE GALLERY POST
     * @param array $data
     */
    public function galeriaPostBKP(array $data): void
    {
        $post = (new Gallery())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            "Confira de imagens",
            url("/fotos/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        $pastinha = explode('.',$post->cover);
        $past = explode('/',$pastinha[0]);
        $past = array_pop($past);
        $folder = str_replace($past,'',$pastinha[0]);

        $arch = glob(getcwd().'/'.CONF_UPLOAD_DIR.'/'.$folder.$post->uri.'/*.*');
        $pager = new Pager(url("/fotos/{$post->uri}/p/"));
        $pager->pager(count($arch), 10, ($data['page'] ?? 1));

        $cont_arch = ($arch == null)? ['no_image'] : $arch ;

        $atual = ($data['page'] ?? 1);
        $pagArch = array_chunk($cont_arch, $pager->limit());
        $resultado  = $pagArch[$atual-1];

        echo $this->view->render("galeria-post", [
            "head" => $head,
            "post" => $post,
            "cor" => (new Category())->findByUri("entretenimento")->color,
            "count" => count($arch),
            "image" => $resultado,
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE VIDEOS ENTRETENIMENTO
     * @param array|null $data
     */
    public function videos(?array $data): void
    {
        $head = $this->seo->render(
            "Vídeos de Entretenimento - " . CONF_SITE_NAME,
            "Confira as notícias de entretenimento atualizada a todo momento",
            url("/video-entretenimento"),
            theme("/assets/images/share.jpg")
        );

        //seleciona o parent da categoria
        $p_arr = (new Category())->find("id = :p || parent = :p", "p=36")->fetch(true);
        $ids = [];
        foreach($p_arr as $ca){
            $ids[] = $ca->id;
        }
        $i_x = implode(',',$ids);

        $blog = (new Post())->findPost("type = :t && category IN({$i_x})", "t=post");
        $pager = new Pager(url("/video-entretenimento/p/"));
        $pager->pager($blog->count(), 10, ($data['page'] ?? 1));

        echo $this->view->render("ent/artigos", [
            "head" => $head,
            "cor" => self::cor(),
            "listNews" => $blog->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE ARTIGOS
     * @param array|null $data
     */
    public function artigo(?array $data): void
    {
        $head = $this->seo->render(
            "Artigo de Entretenimento - " . CONF_SITE_NAME,
            "Confira as notícias de entretenimento atualizada a todo momento",
            url("/artigos-entretenimento"),
            theme("/assets/images/share.jpg")
        );

        //seleciona o parent da categoria
        $p_arr = (new Category())->find("id = :p || parent = :p", "p=10")->fetch(true);
        $ids = [];
        foreach($p_arr as $ca){
            $ids[] = $ca->id;
        }
        $i_x = implode(',',$ids);

        $blog = (new Post())->findPost("type = :t && category IN({$i_x})", "t=post");
        $pager = new Pager(url("/artigos-entretenimento/p/"));
        $pager->pager($blog->count(), 12, ($data['page'] ?? 1));

        echo $this->view->render("ent/artigos", [
            "head" => $head,
            "cor" => self::cor(),
            "listNews" => $blog->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render(),
            "videosEntrLink" => self::videoModal(),
            "postsRecentes" => (new Post())
                ->findPost("type = :t && category NOT IN(10,35,36)", "t=post")
                ->order("post_at DESC")
                ->limit(5)
                ->fetch(true),
            "maisVisitados" => (new Post())
                ->findPost("type = :t  && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
                ->order("views DESC")
                ->limit(5)
                ->fetch(true),
        ]);
    }

    /**
     * SITE ARTIGO POST
     * @param array $data
     */
    public function artigoPost(array $data): void
    {
        $post = (new Post())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            $post->subtitle,
            url("/artigos-entretenimento/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        //seleciona o parent da categoria
        $p_arr = (new Category())->find("id = :p || parent = :p", "p=10")->fetch(true);
        $ids = [];
        foreach($p_arr as $ca){
            $ids[] = $ca->id;
        }
        $i_x = implode(',',$ids);

        $catArray = (new Category())->find("parent = :p || id = :id","p={$post->category()->id}&id={$post->category()->id}")->fetch(true);
        foreach($catArray as $cat){
            $catArray2[] = $cat->id; 
        }
        $cAr = implode(',',$catArray2);
        
        $type = ($post->type == 'post')? 'post' : 'column';
        echo $this->view->render("ent/artigo-post", [
            "head" => $head,
            "post" => $post,
            "galleryId" => ($post->gallery != NULL)? (new Gallery())->findById($post->gallery) : false,
            "idd" => $post->category,
            "cor" => $post->category()->color,
            "videosEntrLink" => self::videoModal(),
            "postsRecentes" => (new Post())
                ->findPost("type = :t && category NOT IN(10,35,36)", "t=post")
                ->order("post_at DESC")
                ->limit(5)
                ->fetch(true),
            "maisVisitados" => (new Post())
                ->findPost("type = :t && category IN({$i_x}) && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
                ->order("views DESC")
                ->limit(5)
                ->fetch(true),
            "latest" => (new Post())
                ->findPost('type = :t', 't=post')
                ->order('post_at DESC')
                ->limit(5)
                ->fetch(true),
            "related" => (new Post())
                ->findPost("category = :c AND id != :i && type = :t", "c={$post->category}&i={$post->id}&t={$type}")
                ->order("RAND()")
                ->limit(18)
                ->fetch(true),
            "list_agenda" => (new Agenda())
                ->findAgenda('event_date >= NOW()')
                ->order("event_date ASC")
                ->limit(5)
                ->fetch(true),
            "ultimasEstado" => (new Post())
                ->findPost("type = :t && category IN({$cAr})", "t=post")
                ->order("post_at DESC")
                ->limit(4)
                ->fetch(true),
            // "poll" => (new Channel())
            //     ->find("status = :status && created_at < NOW() && expire_at > NOW()", "status=post")
            //     ->order("RAND()")
            //     ->fetch(),
            "maislidas" => (new Post())
                ->findPost("type = :t && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
                ->order("views DESC")
                ->limit(10)
                ->fetch(true)
        ]);
    }


    /**
     * SITE AGENDA
     * @param array|null $data
     */
    public function agenda(?array $data): void
    {
        $head = $this->seo->render(
            "Agenda - " . CONF_SITE_NAME,
            "Confira as notícias de nossa região atualizada a todo momento",
            url("/agenda"),
            theme("/assets/images/share.jpg")
        );

        $blog = (new Agenda())->findAgenda('event_date >= NOW()');
        $pager = new Pager(url("/agenda/p/"));
        $pager->pager($blog->count(), 16, ($data['page'] ?? 1));

        echo $this->view->render("ent/agenda", [
            "head" => $head,
            "cor" => self::cor(),
            "list_agenda" => $blog->order("event_date ASC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->render()
        ]);
    }

    /**
     * SITE ARTIGO POST
     * @param array $data
     */
    public function eventoPost(array $data): void
    {
        $post = (new Agenda())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        //seleciona o parent da categoria
        $p_arr = (new Category())->find("id = :p || parent = :p", "p=10")->fetch(true);
        $ids = [];
        foreach($p_arr as $ca){
            $ids[] = $ca->id;
        }
        $i_x = implode(',',$ids);

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            $post->details,
            url("/evento/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );
        
        echo $this->view->render("ent/evento", [
            "head" => $head,
            "post" => $post,
            "videosEntrLink" => self::videoModal(),
            "postsRecentes" => (new Post())
                ->findPost("type = :t && category NOT IN(10,35,36)", "t=post")
                ->order("post_at DESC")
                ->limit(5)
                ->fetch(true),
            "maisVisitados" => (new Post())
                ->findPost("type = :t && category IN({$i_x}) && MONTH(post_at) = MONTH(NOW()) && YEAR(post_at) = YEAR(NOW())", "t=post")
                ->order("views DESC")
                ->limit(5)
                ->fetch(true)
            // "cor" => self::cor()
            // "menuHome" => 'active',
        ]);
    }

    /**
     * SITE PROMOÇÃO
     * @param array|null $data
     */
    public function promotion(?array $data): void
    {
        $head = $this->seo->render(
            "Promoção - " . CONF_SITE_NAME,
            "Confira as promoções e participe",
            url("/promocao"),
            theme("/assets/images/share.jpg")
        );

        $blog = (new Post())->findPost('type = :t', 't=promotion');
        $pager = new Pager(url("/promotion/p/"));
        $pager->pager($blog->count(), 9, ($data['page'] ?? 1));

        echo $this->view->render("promotion", [
            "head" => $head,
            "article" => $blog->order("post_at DESC")->limit($pager->limit())->offset($pager->offset())->fetch(true),
            "paginator" => $pager->renderw('justify-content-center')
        ]);
    }

    /**
     * SITE PROMOTION POST
     * @param array $data
     */
    public function promotionPost(array $data): void
    {
        $post = (new Post())->findByUri($data['uri']);
        if (!$post) {
            redirect("/404");
        }

        $user = Auth::user();
        if (!$user || $user->level < 5) {
            $post->views += 1;
            $post->save();
        }

        $head = $this->seo->render(
            "{$post->title} - " . CONF_SITE_NAME,
            $post->subtitle,
            url("/promotion/{$post->uri}"),
            ($post->cover ? image($post->cover, 1200, 628) : theme("/assets/images/share.jpg"))
        );

        echo $this->view->render("artigo-post", [
            "head" => $head,
            "post" => $post
        ]);
    }

    /**
     * SITE LOGIN
     * @param null|array $data
     */
    public function login(?array $data): void
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (request_limit("weblogin", 3, 60 * 5)) {
                $json['message'] = $this->message->error("Você já efetuou 3 tentativas, esse é o limite. Por favor, aguarde 5 minutos para tentar novamente!")->render();
                echo json_encode($json);
                return;
            }

            if (empty($data['email']) || empty($data['password'])) {
                $json['message'] = $this->message->warning("Informe seu email e senha para entrar")->render();
                echo json_encode($json);
                return;
            }

            $save = (!empty($data['save']) ? true : false);
            $auth = new Auth();
            $login = $auth->login($data['email'], $data['password'], $save);

            if ($login) {
                $this->message->success("Seja bem-vindo(a) de volta " . Auth::user()->first_name . "!")->flash();
                $json['redirect'] = url("/app");
            } else {
                $json['message'] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Entrar - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/entrar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-login", [
            "head" => $head,
            "cookie" => filter_input(INPUT_COOKIE, "authEmail")
        ]);
    }

    /**
     * SITE PASSWORD FORGET
     * @param null|array $data
     */
    public function forget(?array $data)
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (empty($data["email"])) {
                $json['message'] = $this->message->info("Informe seu e-mail para continuar")->render();
                echo json_encode($json);
                return;
            }

            if (request_repeat("webforget", $data["email"])) {
                $json['message'] = $this->message->error("Ooops! Você já tentou este e-mail antes")->render();
                echo json_encode($json);
                return;
            }

            $auth = new Auth();
            if ($auth->forget($data["email"])) {
                $json["message"] = $this->message->success("Acesse seu e-mail para recuperar a senha")->render();
            } else {
                $json["message"] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Recuperar Senha - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/recuperar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-forget", [
            "head" => $head
        ]);
    }

    /**
     * SITE FORGET RESET
     * @param array $data
     */
    public function reset(array $data): void
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (empty($data["password"]) || empty($data["password_re"])) {
                $json["message"] = $this->message->info("Informe e repita a senha para continuar")->render();
                echo json_encode($json);
                return;
            }

            list($email, $code) = explode("|", $data["code"]);
            $auth = new Auth();

            if ($auth->reset($email, $code, $data["password"], $data["password_re"])) {
                $this->message->success("Senha alterada com sucesso. Vamos controlar?")->flash();
                $json["redirect"] = url("/entrar");
            } else {
                $json["message"] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Crie sua nova senha no " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/recuperar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-reset", [
            "head" => $head,
            "code" => $data["code"]
        ]);
    }

    /**
     * SITE REGISTER
     * @param null|array $data
     */
    public function register(?array $data): void
    {
        if (Auth::user()) {
            redirect("/app");
        }

        if (!empty($data['csrf'])) {
            if (!csrf_verify($data)) {
                $json['message'] = $this->message->error("Erro ao enviar, favor use o formulário")->render();
                echo json_encode($json);
                return;
            }

            if (in_array("", $data)) {
                $json['message'] = $this->message->info("Informe seus dados para criar sua conta.")->render();
                echo json_encode($json);
                return;
            }

            $auth = new Auth();
            $user = new User();
            $user->bootstrap(
                $data["first_name"],
                $data["last_name"],
                $data["email"],
                $data["password"]
            );

            if ($auth->register($user)) {
                $json['redirect'] = url("/confirma");
            } else {
                $json['message'] = $auth->message()->before("Ooops! ")->render();
            }

            echo json_encode($json);
            return;
        }

        $head = $this->seo->render(
            "Criar Conta - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/cadastrar"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("auth-register", [
            "head" => $head
        ]);
    }

    /**
     * SITE OPT-IN CONFIRM
     */
    public function confirm(): void
    {
        $head = $this->seo->render(
            "Confirme Seu Cadastro - " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/confirma"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("optin", [
            "head" => $head,
            "data" => (object)[
                "title" => "Falta pouco! Confirme seu cadastro.",
                "desc" => "Enviamos um link de confirmação para seu e-mail. Acesse e siga as instruções para concluir seu cadastro e comece a controlar com o CaféControl",
                "image" => theme("/assets/images/optin-confirm.jpg")
            ]
        ]);
    }

    /**
     * SITE OPT-IN SUCCESS
     * @param array $data
     */
    public function success(array $data): void
    {
        $email = base64_decode($data["email"]);
        $user = (new User())->findByEmail($email);

        if ($user && $user->status != "confirmed") {
            $user->status = "confirmed";
            $user->save();
        }

        $head = $this->seo->render(
            "Bem-vindo(a) ao " . CONF_SITE_NAME,
            CONF_SITE_DESC,
            url("/obrigado"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("optin", [
            "head" => $head,
            "data" => (object)[
                "title" => "Tudo pronto. Você já pode controlar :)",
                "desc" => "Bem-vindo(a) ao seu controle de contas, vamos tomar um café?",
                "image" => theme("/assets/images/optin-success.jpg"),
                "link" => url("/entrar"),
                "linkTitle" => "Fazer Login"
            ],
            "track" => (object)[
                "fb" => "Lead",
                "aw" => "AW-953362805/yAFTCKuakIwBEPXSzMYD"
            ]
        ]);
    }

    /**
     * SITE TERMS
     */
    public function terms(): void
    {
        $head = $this->seo->render(
            CONF_SITE_NAME . " - Termos de uso",
            CONF_SITE_DESC,
            url("/termos"),
            theme("/assets/images/share.jpg")
        );

        echo $this->view->render("terms", [
            "head" => $head
        ]);
    }

    /**
     * SITE NAV ERROR
     * @param array $data
     */
    public function error(array $data): void
    {
        $error = new \stdClass();

        switch ($data['errcode']) {
            case "problemas":
                $error->code = "OPS";
                $error->title = "Estamos enfrentando problemas!";
                $error->message = "Parece que nosso serviço não está diponível no momento. Já estamos vendo isso mas caso precise, envie um e-mail :)";
                $error->linkTitle = "ENVIAR E-MAIL";
                $error->link = "mailto:" . CONF_MAIL_SUPPORT;
                break;

            case "manutencao":
                $error->code = "OPS";
                $error->title = "Desculpe. Estamos em manutenção!";
                $error->message = "Voltamos logo! Por hora estamos trabalhando para melhorar nosso conteúdo para você controlar melhor as suas contas :P";
                $error->linkTitle = null;
                $error->link = null;
                break;

            default:
                $error->code = $data['errcode'];
                $error->title = "Ooops. Conteúdo indispinível :/";
                $error->message = "Sentimos muito, mas o conteúdo que você tentou acessar não existe, está indisponível no momento ou foi removido :/";
                $error->linkTitle = "Continue navegando!";
                $error->link = url_back();
                break;
        }

        $head = $this->seo->render(
            "{$error->code} | {$error->title}",
            $error->message,
            url("/ops/{$error->code}"),
            theme("/assets/images/share.jpg"),
            false
        );

        echo $this->view->render("error", [
            "head" => $head,
            "error" => $error
        ]);
    }
}