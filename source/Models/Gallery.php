<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class Gallery Active Record Pattern
 *
 * @author Enos Santana <sac@enosdev.com>
 * @package Source\Models
 */
class Gallery extends Model
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct("gallery", ["id"], ["title", "local", "date_at"]);
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return mixed|Model
     */
    public function findGallery(?string $terms = null, ?string $params = null, string $columns = "*")
    {
        $terms = "status = :status AND date_at <= NOW()" . ($terms ? " AND {$terms}" : "");
        $params = "status=post" . ($params ? "&{$params}" : "");

        return parent::find($terms, $params, $columns);
    }

    /**
     * @return string|null
     */
    public function photo(): ?string
    {
        if ($this->cover && file_exists(__DIR__ . "/../../" . CONF_UPLOAD_DIR . "/{$this->cover}")) {
            return $this->cover;
        }

        return null;
    }

    /**
     * @param string $uri
     * @param string $columns
     * @return null|Gallery
     */
    public function findByUri(string $uri, string $columns = "*"): ?Gallery
    {
        $find = $this->find("uri = :uri", "uri={$uri}", $columns);
        return $find->fetch();
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Título, Local e Data são obrigatórios");
            return false;
        }

        /** Gallery Update */
        if (!empty($this->id)) {
            $galleryId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$galleryId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Gallery Create */
        if (empty($this->id)) {

            $galleryId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($galleryId))->data();
        return true;
    }
}