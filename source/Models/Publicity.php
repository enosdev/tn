<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class User Active Record Pattern
 *
 * @author Robson V. Leite <cursos@upinside.com.br>
 * @package Source\Models
 */
class Publicity extends Model
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct("publicity", ["id"], ["name", "page", "local", "publicity_at"]);
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return mixed|Model
     */
    public function findPublicity(?string $terms = null, ?string $params = null, string $columns = "*")
    {
        $terms = "status = :status AND publicity_at <= NOW()" . ($terms ? " AND {$terms}" : "");
        $params = "status=post" . ($params ? "&{$params}" : "");

        return parent::find($terms, $params, $columns);
    }

    /**
     * @return string|null
     */
    public function photo(): ?string
    {
        if ($this->photo && file_exists(__DIR__ . "/../../" . CONF_UPLOAD_DIR . "/{$this->photo}")) {
            return $this->photo;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Nome, Página e Local são obrigatórios");
            return false;
        }

        /** Publicity Update */
        if (!empty($this->id)) {
            $publicityId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$publicityId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Publicity Create */
        if (empty($this->id)) {

            $publicityId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($publicityId))->data();
        return true;
    }
}