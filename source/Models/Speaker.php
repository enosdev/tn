<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class Speaker Active Record Pattern
 *
 * @author Robson V. Leite <cursos@upinside.com.br>
 * @package Source\Models
 */
class Speaker extends Model
{
    /**
     * Speaker constructor.
     */
    public function __construct()
    {
        parent::__construct("speakers", ["id"], ["name", "program", "start_at", "end_at"]);
    }

    /**
     * @param string $name
     * @param string $program
     * @param string $start_at
     * @param string $end_at
     * @param string|null $document
     * @return Speaker
     */
    public function bootstrap(
        string $name,
        string $program,
        string $start_at,
        string $end_at
    ): Speaker {
        $this->name = $name;
        $this->program = $program;
        $this->start_at = $start_at;
        $this->end_at = $end_at;
        return $this;
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return mixed|Model
     */
    public function findSpeaker(?string $terms = null, ?string $params = null, string $columns = "*")
    {
        $hora = date("H:i:s");
        $week = (date("w") == '0') ? "0" : ((date("w") == '6')? '6': '1');
        $terms = "status = :status && start_at <= '{$hora}' && end_at >='{$hora}' && week LIKE '%{$week}%'" . ($terms ? " AND {$terms}" : "");
        $params = "status=active" . ($params ? "&{$params}" : "");

        return parent::find($terms, $params, $columns);
    }

    /**
     * @return string|null
     */
    public function photo(): ?string
    {
        if ($this->photo && file_exists(__DIR__ . "/../../" . CONF_UPLOAD_DIR . "/{$this->photo}")) {
            return $this->photo;
        }

        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Nome, programa, \"inicia em\" e \finaliza em\" são obrigatórios");
            return false;
        }

        /** Speaker Update */
        if (!empty($this->id)) {
            $speakerId = $this->id;

            $this->update($this->safe(), "id = :id", "id={$speakerId}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Speaker Create */
        if (empty($this->id)) {

            $speakerId = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($speakerId))->data();
        return true;
    }
}