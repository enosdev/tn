<?php

namespace Source\Models;

use Source\Core\Model;

/**
 * FSPHP | Class User Active Record Pattern
 *
 * @author Robson V. Leite <cursos@upinside.com.br>
 * @package Source\Models
 */
class Top10 extends Model
{
    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct("top", ["id"], ["title", "link"]);
    }

    /**
     * @param string|null $terms
     * @param string|null $params
     * @param string $columns
     * @return mixed|Model
     */
    public function findTop10(?string $terms = null, ?string $params = null, string $columns = "*")
    {
        $terms = "status = :status" . ($terms ? " AND {$terms}" : "");
        $params = "status=post" . ($params ? "&{$params}" : "");

        return parent::find($terms, $params, $columns);
    }

    /**
     * @return string|null
     */
    public function photo(): ?string
    {
        if ($this->link) {
            $itens = parse_url ($this->link);
            parse_str($itens['query'], $params);
            return "//i1.ytimg.com/vi/{$params['v']}/mqdefault.jpg";
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function photoWeb(): ?string
    {
        if ($this->link) {
            $itens = parse_url ($this->link);
            parse_str($itens['query'], $params);
            return "//i1.ytimg.com/vi/{$params['v']}/default.jpg";
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function linkWeb(): ?string
    {
        if ($this->link) {
            $itens = parse_url ($this->link);
            parse_str($itens['query'], $params);
            return "//www.youtube.com/embed/{$params['v']}?autoplay=1";
        }

        return null;
    }

    /**
     * @return bool
     */
    public function save(): bool
    {
        if (!$this->required()) {
            $this->message->warning("Titulo e link do vídeo são obrigatórios");
            return false;
        }

        /** Top10 Update */
        if (!empty($this->id)) {
            $top10Id = $this->id;

            $this->update($this->safe(), "id = :id", "id={$top10Id}");
            if ($this->fail()) {
                $this->message->error("Erro ao atualizar, verifique os dados");
                return false;
            }
        }

        /** Top10 Create */
        if (empty($this->id)) {

            $top10Id = $this->create($this->safe());
            if ($this->fail()) {
                $this->message->error("Erro ao cadastrar, verifique os dados");
                return false;
            }
        }

        $this->data = ($this->findById($top10Id))->data();
        return true;
    }
}