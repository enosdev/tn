<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Aguarde...</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
        <style>
            body{
                font-family: 'Open Sans', sans-serif;
                background: #1D2025;
                display: flex;
                justify-content: center;
                align-items: center;
                font-size: 1em
            }
            h1{
                color:#fff;
                font-weight: normal
            }
            small{
                color:#fff;
                font-size: .8rem
            }
            div{
                text-align: center
            }
        </style>
    </head>
    <body>
        <div>
            <h1>Atualização de segurança<br>Aguarde...<br><small>PS: Enos aqui, qualquer dúvida entre em contato</small></h1>
        </div>
    </body>
</html>
<?php
    exit;