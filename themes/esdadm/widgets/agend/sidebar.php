<div class="main_sidebar">
    <h3><i class="fas fa-laptop"></i> dash/agenda</h3>
    <p class="dash_content_sidebar_desc">Aqui você gerencia todos as agendas dos eventos...</p>

    <?php if (!empty($post->cover)): ?>
        <img class="radius" style="width: 100%; margin-top: 30px" src="<?= image($post->cover, 300); ?>"/>
    <?php endif; ?>
</div>