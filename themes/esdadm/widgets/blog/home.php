<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fa fa-edit"></i> Artigos</div>
<style>
    .select_categories{
        margin-right:20px
    }
    .select_categories select{
        padding-right:40px;
        outline:none;
    }
</style>
<div class="searsh_form">
    <div class="select_categories app_form">
        <!-- onchange="document.getElementById('form_filter').submit()" -->
        <form id="form_filter" action="<?= url("/".PATH_ADMIN."/blog/home"); ?>">
            <select name="c">
                <option value="null">Selecione uma categoria</option>
                <option value="null">Limpar Seleção</option>
                <?php
                $categoryId = $select_cate;
                $select = function ($value) use ($categoryId) {
                    return ($categoryId == $value ? "selected" : "");
                };
                foreach ($categories as $category): ?>
                    <optgroup label="<?= $category->title; ?>">
                        <option <?= $select($category->id); ?> value="<?= $category->id; ?>"><?= $category->title; ?></option>
                        <?php
                            $parent = (new Source\Models\Category())->find("parent = :p", "p={$category->id}")->order("title")->fetch(true);
                            foreach($parent as $p):
                            ?>
                                <option <?= $select($p->id); ?> value="<?= $p->id; ?>"><?= $p->title; ?></option>
                            <?php endforeach;
                        ?>
                    </optgroup>
                <?php endforeach; ?>
            </select>
            <!-- <button>tse</button> -->
        </form>
    </div>
    <form action="<?= url("/".PATH_ADMIN."/blog/home"); ?>" class="app_search_form">
        <input type="text" name="s" value="<?= $search; ?>" placeholder="Pesquisar Artigos:">
        <button><i class="fas fa-search"></i></button>
    </form>
</div>

<main>
    <?php if (!$posts): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem artigos cadastrados no site.</div>
    <?php else: ?>
        <?php foreach ($posts as $post):
        $postCover = ($post->cover ? image($post->cover, 300) : "");
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=$postCover;?>')"></div>
            <hr class="hr">
            <p class="title"><a target="_blank" href=" <?= url("/artigo/{$post->uri}"); ?>" title="Ver no site">
                    <?php if ($post->post_at > date("Y-m-d H:i:s")): ?>
                        <span><i style="color:var(--color-yellow)" class="far fa-clock"></i> <?= $post->title; ?></span>
                    <?php else: ?>
                        <span><i style="color:var(--color-green)" class="fa fa-check"></i><?= $post->title; ?></span>
                    <?php endif; ?>
                </a>
            </p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= date_fmt($post->post_at, "d.m.y \à\s H\hi"); ?></p>
                <p style="color:<?=$post->category()->color;?>;"><i class="fa fa-tag"></i><?= $post->category()->title; ?></p>
                <p><i class="fas fa-user"></i> <?= $post->author()->fullName(); ?></p>
                <p><i class="fas fa-eye"></i><?= $post->views; ?></p>
                <p><i class="fas fa-share-square"></i><?= ($post->status == "post" ? "<span style='color:var(--color-green)'>Público</span>" : ($post->status == "draft" ? "<span style='color:var(--color-yellow)'>Rascunho</span>" : "<span style='color:var(--color-red)'>Lixo</span>")); ?></p>
                <hr>
                <p>compartilhar</p>
                <p style="display:flex">
                    <a target="_blank" class="btn btn-blue" title="Facebook" style="margin:0 3px; font-size:1em" href="http://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$post->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" data-url="<?= url("/artigo/{$post->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>"><i class="fab fa-facebook-f"></i></a>
                    <a target="_blank" class="btn btn-green" title="WhatsApp" style="margin:0 3px; font-size:1em" href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$post->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!"><i class="fab fa-whatsapp"></i></a>
                    <a target="_blank" class="btn btn-blue" title="Telegram" style="margin:0 3px; font-size:1em" href="https://telegram.me/share/url?url=<?= url("/artigo/{$post->uri}"); ?>&text=<?=$post->title;?>"><i class="fab fa-telegram"></i></a>
                    <a target="_blank" class="btn btn-blue" title="Twitter" style="margin:0 3px; font-size:1em" href="https://twitter.com/intent/tweet?original_referer=<?= url("/artigo/{$post->uri}"); ?>&source=tweetbutton&text=<?=$post->title;?>&url=<?= url("/artigo/{$post->uri}"); ?>&via=<?=CONF_SOCIAL_TWITTER_CREATOR;?>" data-hashtags="" data-url="<?= url("/artigo/{$post->uri}"); ?>" data-via="<?=CONF_SOCIAL_TWITTER_CREATOR;?>" data-text="<?=$post->title;?>"><i class="fab fa-twitter"></i></a>
                </p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" title=""
                    href="<?= url("/".PATH_ADMIN."/blog/post/{$post->id}"); ?>"><i class="fas fa-edit"></i>Editar</a>

                <a class="btn btn-red" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/blog/post"); ?>"
                    data-action="lixeira"
                    data-confirm="Tem certeza que deseja enviar este post para lixeira?"
                    data-post_id="<?= $post->id; ?>"><i class="fa fa-trash-alt"></i>Lixeira</a>
            </div> 
        </div>
        <?php endforeach; ?>
        <div class="clear"></div>
        
        <div class="paginacao">
        <hr class="hr">
            <?= $paginator; ?>
        </div>
    <?php endif; ?>
</main>
