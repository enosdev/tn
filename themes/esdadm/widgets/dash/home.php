<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-home"></i> Dashboard</div>
<main>
    <div class="widgets">
        <p><i class="fas fa-edit"></i> Artigos</p>
        <hr class="hr">
        <p>Artigos: <?= $blog->posts; ?></p>
        <p>Rascunhos: <?= $blog->drafts; ?></p>
        <p>Categorias: <?= $blog->categories; ?></p>
    </div>
    <div class="widgets">
        <p><i class="far fa-chart-bar"></i> Estatisticas Mês Atual</p>
        <hr class="hr">
        <p>Usuários: <?= $statistic->user; ?></p>
        <p>Sessões: <?= $statistic->views; ?></p>
        <p>Páginas vistas: <?= $statistic->pages; ?></p>
    </div>
    <div class="widgets">
        <p><i class="far fa-chart-bar"></i> Estatisticas Diária</p>
        <hr class="hr">
        <p>Usuários: <?= ($now)?$now->users:'0'; ?></p>
        <p>Sessões: <?= ($now)?$now->views:'0'; ?></p>
        <p>Páginas vistas: <?= ($now)?$now->pages:'0'; ?></p>
    </div>
    <div class="widgets">
        <p><i class="fas fa-user-friends"></i> Usuários</p>
        <hr class="hr">
        <!-- <p>Usuários: < ?= $users->users; ?></p> -->
        <p>Admins: <?= $users->admins; ?></p>
        <p>Editores: <?= $users->editores; ?></p>
        <p>Colunistas: <?= $users->colunista; ?></p>
    </div>
    <div class="user_online">
        <h1><i class="far fa-chart-bar fa-2x"></i> Online agora: <span class="app_dash_home_trafic_count"><?= $onlineCount; ?></span></h1>
        <div class="trafic_list app_dash_home_trafic_list">
            <?php if (!$online): ?>
                <div class="message info">
                    <i class="fas fa-info fa-2x"></i> Não existem usuários online navegando no site neste momento. Quando tiver, você
                    poderá monitoriar todos por aqui.
                </div>
            <?php else: ?>
                <?php foreach ($online as $onlineNow): ?>
                    <article>
                        <p>[<?= date_fmt($onlineNow->created_at, "H\hm"); ?> - <?= date_fmt($onlineNow->updated_at,
                                "H\hm"); ?>]
                            <?= ($onlineNow->user ? $onlineNow->user()->fullName() : "Usuário Comum"); ?></p>
                        <p><?= $onlineNow->pages; ?> páginas vistas</p>
                        <p><a target="_blank" href="<?= url("{$onlineNow->url}"); ?>"><i class="fas fa-external-link-alt"></i> <?= $onlineNow->url; ?>
                            </a></p>
                    </article>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
</main>

<?php $v->start("scripts"); ?>
<script>
    $(function () {
        setInterval(function () {
            $.post('<?= url("/".PATH_ADMIN."/dash/home");?>', {refresh: true}, function (response) {
                // count
                if (response.count) {
                    $(".app_dash_home_trafic_count").text(response.count);
                } else {
                    $(".app_dash_home_trafic_count").text(0);
                }

                //list
                var list = "";
                if (response.list) {
                    $.each(response.list, function (item, data) {
                        var url = '<?= url();?>' + data.url;
                        var title = '<?= strtolower(CONF_SITE_NAME);?>';

                        list += "<article>";
                        list += "<p>[" + data.dates + "] " + data.user + "</p>";
                        list += "<p>" + data.pages + " páginas vistas</p>";
                        list += "<p>";
                        list += "<a target='_blank' href='" + url + "'><i class='fas fa-external-link-alt'></i> " + data.url + "</a>";
                        list += "</p>";
                        list += "</article>";
                    });
                } else {
                    list = "<div class=\"message info\">\n" +
                        "<i class=\"fas fa-info fa-2x\"></i> Não existem usuários online navegando no site neste momento. Quando tiver, você\n" +
                        "poderá monitoriar todos por aqui.\n" +
                        "</div>";
                }

                $(".app_dash_home_trafic_list").html(list);
            }, "json");
        }, 1000 * 10);
    });
</script>
<?php $v->end(); ?>

