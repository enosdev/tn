<?php $v->layout("_admin"); ?>
<div class="desc"><i class="fas fa-images"></i> Galeria de imagens</div>


<main>
<?php $v->insert("widgets/gal/sidebar.php"); ?>
    <?php if (!$gall): ?>

        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/gal/gallery"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="create"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Title:</span>
                        <input type="text" name="title" placeholder="Titulo" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Local:</span>
                        <input type="text" name="local" placeholder="Local do evento"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data do evento:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="event_date" value="<?= date('d/m/Y H:i');?>" required/>
                    </label>
                    <label class="label">
                        <span class="legend">Tag:</span>
                        <input type="text" name="tag" placeholder="tag"/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">É entretenimento?:</span>
                        <select name="entertainment" required>
                            <option value="yes">Sim</option>
                            <option value="no">Não</option>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">Data da Publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="date_at" value="<?= date("d/m/Y H:i") ;?>"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                            <?php foreach ($authors as $author):
                                $authorId = user()->id;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "disabled");
                                };
                                ?>
                                <option <?= $select($author->id); ?>
                                        value="<?= $author->id; ?>"><?= $author->fullName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <select name="status" required>
                            <option value="post">Publicar</option>
                            <option value="draft">Rascunho</option>
                            <option value="trash ">Lixo</option>
                        </select>
                    </label>
                </div>

                <div class="al-right">
                    <button class="btn btn-green"><i class="far fa-save"></i>Criar Galeria</button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="main_box">
            <form class="app_form" action="<?= url("/".PATH_ADMIN."/gal/gallery/{$gall->id}"); ?>" method="post">
                <!--ACTION SPOOFING-->
                <input type="hidden" name="action" value="update"/>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Title:</span>
                        <input type="text" name="title" placeholder="Título" value="<?= $gall->title; ?>" required/>
                    </label>

                    <label class="label">
                        <span class="legend">*Local:</span>
                        <input type="text" name="local" placeholder="Local do evento" value="<?= $gall->local; ?>"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">Data do Evento:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="event_date" value="<?= date("d/m/Y H:i", strtotime($gall->event_date)) ;?>"/>
                    </label>
                    <label class="label">
                        <span class="legend">Tag:</span>
                        <input type="text" name="tag" value="<?=$gall->tag ;?>"/>
                    </label>
                </div>

                <label class="label">
                    <span class="legend">Imagem:</span>
                    <input id="file" type="file" name="photo"/>
                </label>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">É entretenimento?:</span>
                        <?php
                        $sta = $gall->entertainment;
                        $select = function ($value) use ($sta) {
                            return ($sta == $value ? "selected" : "");
                        };
                        ?>
                        <select name="entertainment" required>
                            <option value="yes" <?=$select('yes');?>>Sim</option>
                            <option value="no" <?=$select('no');?>>Não</option>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">Data da Publicação:</span>
                        <input type="text" class="mask-datetime datetimepicker" name="date_at" value="<?= date("d/m/Y H:i", strtotime($gall->date_at)) ;?>"/>
                    </label>
                </div>

                <div class="label_g2">
                    <label class="label">
                        <span class="legend">*Autor:</span>
                        <select name="author" required>
                            <?php foreach ($authors as $author):
                                $authorId = $gall->author;
                                $select = function ($value) use ($authorId) {
                                    return ($authorId == $value ? "selected" : "disabled");
                                };
                                ?>
                                <option <?= $select($author->id); ?>
                                        value="<?= $author->id; ?>"><?= $author->fullName(); ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                    <label class="label">
                        <span class="legend">*Status:</span>
                        <?php
                        $sta = $gall->status;
                        $select = function ($value) use ($sta) {
                            return ($sta == $value ? "selected" : "");
                        };
                        ?>
                        <select name="status" required>
                            <option value="post" <?=$select('post');?>>Publicar</option>
                            <option value="draft" <?=$select('draft');?>>Rascunho</option>
                            <option value="trash" <?=$select('trash');?>>Lixo</option>
                        </select>
                    </label>
                </div>


                <div class="app_form_footer">
                    <button class="btn btn-blue"><i class="fa fa-sync"></i>Atualizar</button>
                    <a href="#" class="remove_link"
                       data-post="<?= url("/".PATH_ADMIN."/gal/gallery/{$gall->id}"); ?>"
                       data-action="delete"
                       data-confirm="ATENÇÃO: Tem certeza que deseja excluir a galeria? Essa ação não pode ser desfeita!"
                       data-gallery_id="<?= $gall->id; ?>"><i class="far fa-trash-alt"></i>Excluir Galeria</a>
                </div>
            </form>
            <hr>
            <div>
                <a class="btn btn-green" href="<?= url("/".PATH_ADMIN."/gal/photos/{$gall->id}"); ?>"> Ver Imagens enviadas</a>
            </div>
            
            <div class="dropzone" id="dropzone" style="display:block">
                <?php
                    $pastinha = explode('.',$gall->cover);
                    $past = explode('/',$pastinha[0]);
                    $past = array_pop($past);
                    $folder = str_replace($past,'',$pastinha[0]);
                ?>
                <input type="hidden" value="<?= $folder.$gall->uri; ?>">
            </div>
        </div>

    <?php endif; ?>
</main>