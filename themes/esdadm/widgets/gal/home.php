<?php $v->layout("_admin"); ?>

<div class="desc"><i class="fas fa-images"></i> Galeria de imagens</div>
<div class="searsh_form">
    <form action="<?= url("/".PATH_ADMIN."/gal/home"); ?>" class="app_search_form">
        <input type="text" name="s" value="<?= $search; ?>" placeholder="Pesquisar Galeria:">
        <button><i class="fas fa-search"></i></button>
    </form>
</div>
<main>
    <?php foreach ($gallery as $gal):
    $galPhoto = ($gal->photo() ? image($gal->cover, 300) : theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
    ?>
    <div class="widgets user-list">
        <div class="cover" style="background-image:url('<?=$galPhoto;?>')"></div>
        <hr class="hr">
        <p><?= $gal->title; ?></p>
        <div class="info_list">
            <p>Data <?= date_fmt($gal->date_at, "d/m/y \à\s H\hi"); ?></p>
            <p><i class="far fa-eye"></i><?= $gal->views; ?></p>
        </div>
        <a class="btn btn-blue" href="<?= url("/".PATH_ADMIN."/gal/gallery/{$gal->id}"); ?>" title=""><i class="fa fa-cogs"></i>Gerenciar</a>    
    </div>
    <?php endforeach; ?>
    <div class="clear"></div>
    
    <div class="paginacao">
    <hr class="hr">
        <?= $paginator; ?>
    </div>
</main>
