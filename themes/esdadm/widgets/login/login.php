<?php $v->layout("_login"); ?>

<div class="login">
    <form name="login" action="<?= url("/".PATH_ADMIN."/login"); ?>" method="post">
        <h1>
            <span class="mascotinho">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 398.14 643.42"><title>login</title><g id="Camada_2" data-name="Camada 2"><g id="svg6639"><path d="M200.54,153.32c38.48,0,72.15-28,72.15-76.36,0-49-33.67-77-72.15-77s-72.15,28-72.15,77C128.39,125.36,162.06,153.32,200.54,153.32Zm0-132.58c28,0,46.6,22.55,46.6,56.22,0,33.37-18.64,55.62-46.6,55.62S154,110.33,154,77C154,43.29,172.58,20.74,200.54,20.74Z"/><path d="M249,307.9c-13.23,8.42-27.06,13.83-44.5,13.83-30.33,0-53.84-16.8-56.79-49.9H262.8a101.59,101.59,0,0,0,1.2-15c0-41.19-24.65-68.55-66.74-68.55-39.08,0-74.56,28.86-74.56,76.36,0,48.71,34.88,77,78.77,77A100.11,100.11,0,0,0,258,324.14ZM197.86,207.79c24.91,0,41.47,14.86,43.64,44.8H148.29C152.87,223.77,174,207.79,197.86,207.79Z"/><polygon points="286.01 181.78 286.01 205.83 376.2 266.55 376.2 267.76 286.01 328.48 286.01 352.53 398.14 276.77 398.14 257.54 286.01 181.78"/><polygon points="112.14 328.48 21.95 267.76 21.95 266.55 112.14 205.83 112.14 181.78 0 257.54 0 276.77 112.14 352.53 112.14 328.48"/><polygon points="50.41 643.42 72.66 643.42 171.26 381.87 149.02 381.87 50.41 643.42"/><polygon points="230.78 381.87 329.39 643.42 351.64 643.42 253.03 381.87 230.78 381.87"/></g></g></svg>
            </span>Login
        </h1>
        <div class="ajax_response"><?= flash(); ?></div>
        <label for="login">
            <p><i class="far fa-envelope"></i> email:</p>
            <input id="login" name="email" type="email" placeholder="informe seu email" required>
        </label>
        <label for="pass">
            <p><i class="fas fa-key"></i> senha:</p>
            <input id="pass" name="password" type="password" placeholder="informe sua senha" required>
        </label>
        <button class="btn btn-green"><i class="fas fa-sign-in-alt fa-2x"></i> ENTRAR</button>
    </form>
    <div class="dev">
        <p>Desenvolvido por www.<strong>estudiofox</strong>.com</p>
        <p>&copy; 2019 - todos os direitos reservados</p>
        <a target="_blank" class="link_whats" href="https://api.whatsapp.com/send?phone=5573998690564&amp;text=Olá, preciso de ajuda com o login.">
            <i class="fab fa-whatsapp"></i> WhatsApp: (73) 99869 0564
        </a>
    </div>
</div>