<?php $v->layout("_admin"); ?>
<div class="desc"><i class="far fa-trash-alt"></i> Lixeira</div>

<div style="background:var(--color-red);justify-content:left;padding:10px 20px;margin:30px 0 10px 0;color:#fff">
    <i class="far fa-edit"></i>Artigos
</div>

<main>
    <?php if (!$posts): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem artigos no lixo.</div>
    <?php else: ?>
        <?php foreach ($posts as $post):
        $postCover = ($post->cover ? image($post->cover, 300) : "");
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=$postCover;?>')"></div>
            <hr class="hr">
            <p class="title"><a target="_blank" href=" <?= url("/artigo/{$post->uri}"); ?>" title="Ver no site">
                    <?php if ($post->post_at > date("Y-m-d H:i:s")): ?>
                        <span><i style="color:var(--color-yellow)" class="far fa-clock"></i> <?= $post->title; ?></span>
                    <?php else: ?>
                        <span><i style="color:var(--color-green)" class="far fa-check"></i><?= $post->title; ?></span>
                    <?php endif; ?>
                </a>
            </p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= date_fmt($post->post_at, "d.m.y \à\s H\hi"); ?></p>
                <p style="color:<?=$post->category()->color;?>;"><i class="far fa-tag"></i><?= $post->category()->title; ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" title=""
                    href="<?= url("/".PATH_ADMIN."/blog/post/{$post->id}"); ?>"><i class="fas fa-edit"></i>Editar</a>

                <a class="btn btn-red" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/blog/post"); ?>"
                    data-action="delete"
                    data-confirm="Tem certeza que deseja deletar este post definitivamente?"
                    data-post_id="<?= $post->id; ?>"><i class="far fa-trash-alt"></i>Deletar</a>
            </div> 
        </div>
        <?php endforeach; ?>
    <?php endif; ?>
</main>


<div style="background:var(--color-red);justify-content:left;padding:10px 20px;margin:30px 0 10px 0;color:#fff">
    <i class="fas fa-user-edit"></i>Colunas
</div>

<main>
    <?php if (!$column): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem artigos no lixo.</div>
    <?php else: ?>
        <?php foreach ($column as $post):
        $postCover = ($post->cover ? image($post->cover, 300) : "");
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=$postCover;?>')"></div>
            <hr class="hr">
            <p class="title"><a target="_blank" href=" <?= url("/artigo/{$post->uri}"); ?>" title="Ver no site">
                    <?php if ($post->post_at > date("Y-m-d H:i:s")): ?>
                        <span><i style="color:var(--color-yellow)" class="far fa-clock"></i> <?= $post->title; ?></span>
                    <?php else: ?>
                        <span><i style="color:var(--color-green)" class="far fa-check"></i><?= $post->title; ?></span>
                    <?php endif; ?>
                </a>
            </p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= date_fmt($post->post_at, "d.m.y \à\s H\hi"); ?></p>
                <p style="color:<?=$post->category()->color;?>;"><i class="far fa-tag"></i><?= $post->category()->title; ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" title=""
                    href="<?= url("/".PATH_ADMIN."/column/post/{$post->id}"); ?>"><i class="fas fa-edit"></i>Editar</a>

                <a class="btn btn-red" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/column/post"); ?>"
                    data-action="delete"
                    data-confirm="Tem certeza que deseja deletar este post definitivamente?"
                    data-post_id="<?= $post->id; ?>"><i class="far fa-trash-alt"></i>Deletar</a>
            </div> 
        </div>
        <?php endforeach; ?>
    <?php endif; ?>
</main>



<div style="background:var(--color-red);justify-content:left;padding:10px 20px;margin:30px 0 10px 0;color:#fff">
    <i class="far fa-images"></i>Galeria
</div>

<main>
    <?php if (!$gallery): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem galerias no lixo.</div>
    <?php else: ?>
        <?php foreach ($gallery as $gal):
        $galPhoto = ($gal->photo() ? image($gal->cover, 300) : theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=$galPhoto;?>')"></div>
            <hr class="hr">
            <p><?= $gal->title; ?></p>
            <div class="info_list">
                <p>Data <?= date_fmt($gal->date_at, "d/m/y \à\s H\hi"); ?></p>
                <p><i class="far fa-eye"></i><?= $gal->views; ?></p>
            </div>
            <a class="btn btn-blue" href="<?= url("/".PATH_ADMIN."/gal/gallery/{$gal->id}"); ?>" title=""><i class="fa fa-cogs"></i>Gerenciar</a>    
        </div>
        <?php endforeach; ?>
    <?php endif; ?>
</main>


<div style="background:var(--color-red);justify-content:left;padding:10px 20px;margin:30px 0 10px 0;color:#fff">
    <i class="far fa-image"></i>Publicidade
</div>

<main>
    <?php if (!$ads): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem Publicidades cadastrada.</div>
    <?php else: ?>
        <?php foreach ($ads as $publicity):
        $publicityPhoto = ($publicity->photo() ? image($publicity->photo, 300, 150) :
        theme("/assets/images/avatar.jpg", CONF_VIEW_ADMIN));
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?= $publicityPhoto; ?>')"></div>
            <hr class="hr">
            <p class="title" style="font-size:var(--font-medium)"><?=$publicity->name;?></p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= ($publicity->publicity_at > date("Y-m-d H:i:s"))? '<span style=color:blue>Agendado para: '.date_fmt($publicity->publicity_at, "d.m.y \à\s H\hi").'</span>': date_fmt($publicity->publicity_at, "d.m.y \à\s H\hi"); ?></p>
                <p class="publi-out"><i class="far fa-eye-slash"></i><?= date_fmt($publicity->publicity_out, "d.m.y \à\s H\hi"); ?></p>
                
                <?php
                    if(date('Y-m-d H:i:s') > $publicity->publicity_out){?>
                       <p class="conta-hora"><i class="fas fa-times-circle"></i> Expirado</p>
                    <?php } else {?>
                        <p class="conta-hora-green"><i class="fas fa-check"></i><strong>Expira:</strong> <?=dataPublicidade($publicity->publicity_out);?></p>
                   <?php }
                    $pagina = str_replace(['300x600','300x250','728x90','220x90','popup'],[' 300x600',' 300x250',' 728x90',' 220x90',' popup'],$publicity->page);
                    $pagina = explode(' ',current(explode(',', $pagina)));
                ?>
                <p class="local-list"><i class="far fa-images"></i> <strong>Mídia:</strong> <?=end($pagina);?></p>
                <p><i class="fas fa-share-square"></i><?= ($publicity->status == "post" ? "<span style='color:var(--color-green)'>Público</span>" : ($publicity->status == "draft" ? "<span style='color:var(--color-yellow)'>Rascunho</span>" : "<span style='color:var(--color-red)'>Lixo</span>")); ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" href="<?= url("/".PATH_ADMIN."/ads/publicity/{$publicity->id}"); ?>" title=""><i class="fa fa-cogs"></i>Gerenciar</a>
            </div> 
        </div>
        <?php endforeach; ?>
    <?php endif; ?>
</main>


<div style="background:var(--color-red);justify-content:left;padding:10px 20px;margin:30px 0 10px 0;color:#fff">
    <i class="far fa-file-alt"></i>Páginas
</div>

<main>
    <?php if (!$pages): ?>
        <div class="message info"><i class="fas fa-info fa-2x"></i>Ainda não existem artigos no lixo.</div>
    <?php else: ?>
        <?php foreach ($pages as $post):
        $postCover = ($post->cover ? image($post->cover, 300) : "");
        ?>
        <div class="widgets user-list">
            <div class="cover" style="background-image:url('<?=$postCover;?>')"></div>
            <hr class="hr">
            <p class="title"><a target="_blank" href=" <?= url("/artigo/{$post->uri}"); ?>" title="Ver no site">
                    <?php if ($post->post_at > date("Y-m-d H:i:s")): ?>
                        <span><i style="color:var(--color-yellow)" class="far fa-clock"></i> <?= $post->title; ?></span>
                    <?php else: ?>
                        <span><i style="color:var(--color-green)" class="far fa-check"></i><?= $post->title; ?></span>
                    <?php endif; ?>
                </a>
            </p>
            <div class="info_list">
                <p><i class="far fa-clock"></i><?= date_fmt($post->post_at, "d.m.y \à\s H\hi"); ?></p>
            </div>
            <div class="actions">
                <a class="btn btn-blue" title=""
                    href="<?= url("/".PATH_ADMIN."/pages/post/{$post->id}"); ?>"><i class="fas fa-edit"></i>Editar</a>

                <a class="btn btn-red" title="" href="#"
                    data-post="<?= url("/".PATH_ADMIN."/pages/post"); ?>"
                    data-action="delete"
                    data-confirm="Tem certeza que deseja deletar este post definitivamente?"
                    data-post_id="<?= $post->id; ?>"><i class="far fa-trash-alt"></i>Deletar</a>
            </div> 
        </div>
        <?php endforeach; ?>
    <?php endif; ?>
</main>

