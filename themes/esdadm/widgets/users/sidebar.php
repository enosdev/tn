<div class="main_sidebar">
    <h3><i class="fas fa-laptop"></i> dash/usuários</h3>
    <p class="dash_content_sidebar_desc">Gerencie, monitore e acompanhe os usuários do seu site aqui...</p>

    <?php if (!empty($user) && $user->photo()): ?>
        <p style="text-align:center;margin-top:30px"><?=$user->fullName();?></p>
        <div id="image-holder">
            <img class="radius" style="width: 100%; margin-top: 2px" src="<?= image($user->photo, 240, 200); ?>"/>
        </div>
    <?php else: ?>
        <div id="image-holder"></div>
    <?php endif;?>
</div>
