<?php
    $category_menu = (new Source\Models\Category())
                    ->find('parent IS NULL && menu = :m', 'm=on')
                    ->fetch(true);
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?=$head;?>
    <link rel="site" href="<?= url(); ?>">
    <link rel="stylesheet" href="<?= theme("/assets/css/bootstrap.min.css?v=".CONF_SITE_VERSION); ?>"/>
    <link rel="stylesheet" href="<?= theme("/style.css?v=".CONF_SITE_VERSION); ?>"/>
</head>

<body>
    <div class="container">
        <h1 class="d-none">Teixeira Notícias</h1>
        <div class="mt-2 mb-2">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-12 text-center">
                    <div style="font-size: 4em;">
                        <a href="<?= url(); ?>" title=""><img class="img-fluid" src="<?= theme("/assets/img/logo-teixeira-noticias.png");?>" alt="Teixeira Notícias" title="Início"/></a>
                    </div>
                </div>
            </div>
        </div><!-- /header -->
        <nav class="navbar navbar-expand-lg nav position-relative">
            <!-- <a class="navbar-brand" href="#">Navbar</a> -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Alterna navegação">
                <!-- <span class="navbar-toggler-icon"></span> -->
                <i class="menu">menu</i>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                    <a class="nav-item nav-link <?=$menuHome ?? '';?>" href="<?=url();?>">Início</a>
                    <?php
                    //carrega o menu no site
                    if($category_menu):
                        foreach($category_menu as $c):
                            $v->insert("menu.php", ["menu" => $c]);
                        endforeach;
                    endif;
                    ?>
                </div>
            </div>
            <div class="search">
                <form class="form">
                    <input type="search" placeholder="O que procura???">
                    <button><i class="search"></i></button>
                </form>
            </div>
        </nav><!-- /nav -->
    </div>

    <!-- CONTENT -->
    <main role="main" class="container">
        <?= $v->section("content"); ?>
    </main>

    <footer class="container">
        Copyright © 2020 teixeiranoticias.com.br | Todos os direitos reservados
    </footer>

    <script src="<?= theme("/assets/js/jquery.min.js?v=".CONF_SITE_VERSION); ?>"></script>
    <script src="<?= theme("/assets/js/bootstrap.min.js?v=".CONF_SITE_VERSION); ?>"></script>
    <script src="<?= theme("/script.js?v=".CONF_SITE_VERSION); ?>"></script>
</body>

</html>