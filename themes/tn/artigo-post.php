<?php $v->layout("_theme"); ?>

<div class="row">
    <div id="html" class="col-md-8">
        <article>
            <h2><?=$post->title;?></h2>
            <p class="categ"><?=$post->subtitle;?></p>
            <p class="categ"><?=$post->category()->title;?></p>
            <small>por: <?=$post->author()->first_name;?> <?=$post->author()->last_name;?> - <?=dataPost($post->post_at);?></small>
            <div class="texto_html">
                <img class="d-block w-100 img-thumbnail cover_destaque"
                    src="<?= image($post->cover, 730, 400); ?>" alt="<?=$post->title;?>">
                <?=html_entity_decode($post->content);?>
            </div>
            <div class="compartilhar">
                <span>compartilhe</span>
                <a href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$blog->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!" target="_blank">
                    <i class="whatsapp"></i>
                </a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$blog->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" data-url="<?= url("/artigo/{$blog->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" target="_blank">
                    <i class="facebook"></i>
                </a>
            </div>
            <hr>
        </article>

    </div>
    <!-- /.site-main -->
    

    <?php $v->insert("aside", ["mais" => $latest, "tituloLista" => "Últimas postadas"]);?>
    <!-- /.site-aside -->
</div>