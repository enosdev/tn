<?php $v->layout("_theme"); ?>

<div class="row">
    <div id="html" class="col-md-8">
        <div class="row">
            <!-- /.Banner -->
            <div class="col-md-12 banner">
                <small>publicidade</small>
                <img class="img-fluid" src="https://picsum.photos/id/800/730/130" alt="">
            </div>
        </div>
        <hr>
        <div class="artigos text-center">
            <h1><?=$title;?></h1>
            <small><?=$desc;?></small>
        </div>
        <hr>

        <?php if($article):
            foreach($article as $blog):
        ?>
        <article>
            <a href="<?=url("/artigo/{$blog->uri}");?>" title="<?=$blog->title;?>">
                <h2><?=$blog->title;?></h2>
            </a>
            <div class="texto_html">
                <div class="row">
                    <div class="col-md-6">
                        <img class="d-block w-100 img-thumbnail cover_destaque" src="<?= image($blog->cover, 300, 150); ?>" alt="<?=$blog->title;?>">
                    </div>
                    <div class="col-md-6">
                        <?=str_limit_chars(strip_tags($blog->content),120);?>
                    </div>
                </div>
            </div>
            <div style="clear:both;"></div>
            <a class="veja_mais" href="<?= url("/artigo/{$blog->uri}"); ?>">veja +</a>
            <div class="compartilhar">
                <a href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$blog->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!" target="_blank">
                    <i class="whatsapp"></i>
                </a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$blog->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" data-url="<?= url("/artigo/{$blog->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" target="_blank">
                    <i class="facebook"></i>
                </a>
            </div>
            <hr>
        </article>

        <?php endforeach;
        endif;
        ?>

    </div>
    <!-- /.site-main -->

    <?php $v->insert("aside", ["mais" => $maisLidas, "tituloLista" => "As mais lidas"]);?>
    <!-- /.site-aside -->
</div>