<aside class="col-md-4">
    <div class="banne-aside">
        <small>publicidade</small>
        <img class="img-fluid" src="https://picsum.photos/id/237/750/400" alt="">
    </div>
    <div class="mais-lidas">
        <h3><?=$tituloLista;?></h3>

        <?php
            if($mais):
                foreach($mais as $pm): ?>
                    <a class="list-lidas" href="<?=url("/artigo/{$pm->uri}");?>">
                        <img src="<?= image($pm->cover, 80, 80); ?>" alt="">
                        <h4><?=str_limit_chars($pm->title,100);?></h4>
                    </a>
        <?php   endforeach;
            endif;
        ?>
    </div>
    <div class="banne-aside">
        <small>publicidade</small>
        <img class="img-fluid" src="https://picsum.photos/id/900/750/400" alt="">
    </div>
</aside>