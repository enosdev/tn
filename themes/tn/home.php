<?php $v->layout("_theme"); ?>

<div class="row">
    <div id="html" class="col-md-8">
        <div class="destaque">
            <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                <?php
                    if($slide):
                        $i = 0;
                        foreach($slide as $sl):
                            $active = ( $i == 0 ? 'class="active"' : '' );?>
                            <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i;?>" <?=$active;?>></li>
                    <?php $i++;
                        endforeach;
                    endif;?>
                </ol>
                <div class="carousel-inner">
                    <?php 
                        if($slide):
                            $is = 0;
                            foreach($slide as $sld):
                                $is++;
                                $act = ($is == 1)? 'active':'';?>

                                <div class="carousel-item <?=$act;?>">
                                    <a href="<?= url("/artigo/{$sld->uri}"); ?>" title="<?=$sld->title;?>">
                                    <img class="d-block w-100" src="<?= image($sld->cover, 730, 400); ?>" alt="<?=$sld->title;?>">
                                    </a>
                                    <a href="<?= url("/artigo/{$sld->uri}"); ?>" title="<?=$sld->title;?>" class="carousel-caption d-none d-md-block fundo-texto">
                                        <h1><?=$sld->title;?></h1>
                                    </a>
                                </div>
                    <?php
                            endforeach;
                        endif;?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                    data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                    data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Próximo</span>
                </a>
            </div>
        </div>

        <div class="row">
            <?php
                if($slideMore):
                    foreach($slideMore as $sm):?>
                    <div class="col-md-4 box-menor">
                        <a href="<?= url("/artigo/{$sm->uri}"); ?>" title="<?=$sm->title;?>">
                            <img class="img-fluid" src="<?= image($sm->cover, 730, 400); ?>" alt="<?=$sm->title;?>">
                            <h2><?=$sm->title;?></h2>
                        </a>
                    </div>
                    <?php endforeach;
                endif;
            ?>

            <!-- /.Banner -->
            <div class="col-md-12 banner">
                <small>publicidade</small>
                <img class="img-fluid" src="https://picsum.photos/id/800/730/130" alt="">
            </div>
        </div>
        <hr>

        <?php if($listaBlog):
            foreach($listaBlog as $blog):
        ?>
        <article>
            <a href="<?=url("/artigo/{$blog->uri}");?>">
                <h2><?=$blog->title;?></h2>
            </a>
            <p class="categ"><?= $blog->category()->title;?> - <small><?= $blog->tag; ?></small></p>
            <small>por: <?=$blog->author()->first_name;?> - <?=dataPost($blog->post_at);?></small>
            <div class="texto_html">
                <img class="d-block w-100 img-thumbnail cover_destaque"
                    src="<?= image($blog->cover, 730, 400); ?>" alt="<?=$blog->title;?>">
                    <?=html_entity_decode(str_limit_words(strip_tags($blog->content, '<p>'),100));?>
            </div>
            <a class="veja_mais" href="<?= url("/artigo/{$blog->uri}"); ?>" title="<?=$blog->title;?>">veja +</a>
            <div class="compartilhar">
                <a href="https://api.whatsapp.com/send?text=Veja esta matéria: <?= url("/artigo/{$blog->uri}"); ?> - É notícia? Tá no <?=CONF_SITE_NAME;?> | <?=CONF_SITE_TITLE;?>!" target="_blank">
                    <i class="whatsapp"></i>
                </a>
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?= url("/artigo/{$blog->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" data-url="<?= url("/artigo/{$blog->uri}"); ?>&app_id=<?=CONF_SOCIAL_FACEBOOK_APP;?>" target="_blank">
                    <i class="facebook"></i>
                </a>
            </div>
            <hr>
        </article>

        <?php endforeach;
        endif;
        ?>

    </div>
    <!-- /.site-main -->

    <?php $v->insert("aside", ["mais" => $maisLidas, "tituloLista" => "As mais lidas"]);?>
    <!-- /.site-aside -->
</div>