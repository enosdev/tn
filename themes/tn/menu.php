<?php
    //função para dexar o menu selecionado
    if(isset($idd)){
        $mm = (new Source\Models\Category())
            ->find('id = :p', "p={$idd}", "parent")
            ->fetch()->parent;
    }
    $categoryId = ($mm ?? '')? $mm : $idd ?? '';
    $activeHome = function ($value) use ($categoryId) {
        return ($categoryId == $value ? "active" : "");
    };
?>
<a class="nav-item nav-link <?=$activeHome($menu->id);?>" href="<?=url("/artigo/em/{$menu->uri}");?>"><?=$menu->title;?></a>