jQuery(function ($) {

	var base = $("link[rel='site']").attr("href");

	$('.link').on('click', function () {
		var link = $(this).attr('data-link');
		var target = $(this).attr('data-target');
		if (target != undefined) {
			window.open(link, target);
		} else {
			window.location.href = link;
		}

	});

	// gambiarra
	// var html = $('article').html();
	// for (i = 0; i < 2; i++) {
	// 	var num = i + 5 + '00';
	// 	$('#html').append('<article id="id' + i + '">' + html + '</article>');
	// 	$('article#id' + i + ' img').attr('src', 'https://picsum.photos/id/' + num + '/730/400');
	// }

	//importando o svg dos icones
	$('i.menu').load(base + '/themes/tn/assets/img/icon/bars-solid.svg');
	$('i.search').load(base + '/themes/tn/assets/img/icon/search.svg');
	$('i.facebook').load(base + '/themes/tn/assets/img/icon/facebook.svg');
	$('i.whatsapp').load(base + '/themes/tn/assets/img/icon/whatsapp.svg');
});
